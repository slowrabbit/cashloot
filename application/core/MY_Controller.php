<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	var $tpl;
	var $currentRoute;

	function __construct(){
		parent::__construct();
		

		$this->load->helper(array('url', 'form'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters("<li>", "</li>");
		$this->lang->load('messages');
		

		$link['create'] = site_url("createAccount");
		$link['transfer'] = site_url("transfer");
		$link['list'] = site_url("accounts");
		$this->currentRoute = "{$this->router->class}/{$this->router->method}";

		$this->tpl['menu'] = $this->load->view('layouts/menu', array(
			"link" => $link,
			"currentRoute" => $this->currentRoute,
		), true);
		
		$this->tpl['head'] = $this->load->view('layouts/head', '', true);
		$this->tpl['footer'] = $this->load->view('layouts/footer', '', true);

		
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	

	protected function _render()
	{
		$this->load->view('layouts/main', array(
			"tpl" => $this->tpl,
		));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
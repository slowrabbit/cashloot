<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define ("BIG_INT", "9223372036854775807");

class Account_model extends CI_Model {

	var $acc_id;
	var $owner_name;
	var $balance;

	function __construct()
    {
        parent::__construct();
    }

    public function add($accountInfo)
    {
    	$this->acc_id   = $accountInfo['serial'];
        $this->owner_name = $accountInfo['client'];
        $this->balance    = $accountInfo['balance']*100;

        $this->db->insert('account', $this);
    }

    public function getAccountHistory($trgAccId)
    {
        $queryStr = "SELECT transfer_history.*, account.owner_name
        FROM transfer_history 
        LEFT JOIN account
        ON transfer_history.src_acc_id = account.acc_id
        WHERE trg_acc_id = {$trgAccId}
        ORDER BY time DESC";

        $query = $this->db->query($queryStr);

        return $query->result_array();
    }

    public function getFormatAccountHistory($trgAccId)
    {
        $accHistory = $this->getAccountHistory($trgAccId);
        foreach($accHistory as $key => $value)
        {
            $plusMinus = ($accHistory[$key]['sum'] > 0) ? "+" : "-";
            $accHistory[$key]['sum'] = $plusMinus.number_format(abs($accHistory[$key]['sum'])/100, 2, '.', '');

            $accHistory[$key]['balance'] = number_format($accHistory[$key]['balance']/100, 2, '.', '');
            $accHistory[$key]['time'] = date('d.m.y/h:i:s', strtotime($accHistory[$key]['time']));
        }
        return $accHistory;
    }

    public function getList()
    {
        $query = $this->db->query("SELECT * FROM account WHERE (acc_id <> ".$this->config->item("sys_account_id").") ORDER BY create_time DESC");

        return $query->result_array();
    }

    public function getFormatList()
    {
        $accList = $this->getList();
        foreach($accList as $key => $value)
        {
            $accList[$key]['balance'] = number_format($accList[$key]['balance']/100, 2, '.', '');
            $accList[$key]['create_time'] = date('d.m.y/h:i:s', strtotime($accList[$key]['create_time']));
        }
        return $accList;
    }

    public function getSysAccount()
    {
        return $this->_getAccById($this->config->item("sys_account_id"));
    }

    public function getAccount($id)
    {
        return $this->_getAccById($id);
    }

    public function isUniqAccId($checkId)
    {
        $query = $this->db->get_where('account', array('acc_id' => $checkId), 1);
        if($query->num_rows() > 0) return FALSE;
        return TRUE;
    }

    public function transfer($fromId, $toId, $sum)
    {
        $transferResult = array(
            'result' => false,
        );

        $this->db->trans_begin();

        $newBalance = $this->_transferAction($fromId, $toId, -$sum);
        if(($newBalance < 0) or ($newBalance > BIG_INT))
        {
            $transferResult['error'] = ERR_TRANSFER_ACCOUNT_DEFICIENCY;
            $this->db->trans_rollback();
        }
        else
        {
            $newBalance = $this->_transferAction($toId, $toId, $sum);
            if(($newBalance < 0) or ($newBalance > BIG_INT))
            {
                $transferResult['error'] = ERR_TRANSFER_ACCOUNT_OVERFLOW;
                $this->db->trans_rollback();
            }
            else
            {
                $this->_transferLog($fromId, $toId, $sum);
                //$fromAccountInfo = $this->_getAccById($fromId);

                $commissionSum = ceil($sum * $this->config->item("trans_commiss_percent") / 100);

                $newBalance = $this->_transferAction($fromId, $this->config->item("sys_account_id"), -$commissionSum);
                if(($newBalance < 0) or ($newBalance > BIG_INT))
                {
                    $transferResult['error'] = ERR_TRANSFER_ACCOUNT_DEFICIENCY;
                    $this->db->trans_rollback();
                   
                }
                else
                {
                    $newBalance = $this->_transferAction($this->config->item("sys_account_id"), $fromId, $commissionSum);
                    if(($newBalance < 0) or ($newBalance > BIG_INT))
                    {
                        $transferResult['error'] = ERR_TRANSFER_ACCOUNT_OVERFLOW;
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $this->_transferLog($fromId, $this->config->item("sys_account_id"), $commissionSum);
                        $transferResult['result'] = true;
                    }
                    
                }
            }
        }
        $this->db->trans_commit();

        

        return $transferResult;
    }

    protected function _transferAction($trgId, $srcId, $sum)
    {
        $accountInfo = $this->_getAccById($trgId);
        $newBalance = $accountInfo['balance'] + $sum;
        $this->_updateBalanceById($trgId, $newBalance);
        $this->_transferLogHistory($trgId, $srcId, $sum, $newBalance);
        return $newBalance;
    }

    protected function _transferLogHistory($trgAccId, $srcAccId, $sum, $balance)
    {
        $this->db->query("INSERT INTO transfer_history (trg_acc_id, src_acc_id, sum, balance) VALUES ({$trgAccId}, {$srcAccId}, {$sum}, {$balance})");
    }

    protected function _transferLog($fromAccId, $toAccId, $sum)
    {
        $this->db->query("INSERT INTO transfer (from_acc_id, to_acc_id, sum) VALUES ({$fromAccId}, {$toAccId}, {$sum})");
    }

    protected function _updateBalanceById($id, $newBalance)
    {
        $this->db->query("UPDATE account SET balance = $newBalance WHERE (acc_id = {$id})");
    }

    protected function _getAccById($id)
    {
        $query = $this->db->query("SELECT * FROM account WHERE acc_id = {$id}");
        return $query->row_array();
    }
}
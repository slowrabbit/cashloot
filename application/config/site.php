<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define("ERR_TRANSFER_ACCOUNT_DEFICIENCY", -1);
define("ERR_TRANSFER_ACCOUNT_OVERFLOW", 1);

$config['domain_name'] = "cashloot.me";
$config['sys_account_id'] = 0;
$config['trans_commiss_percent'] = 0.99;
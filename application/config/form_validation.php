<?php defined('BASEPATH') OR exit('No direct script access allowed');
$config = array(
    'account/create' => array(
        array(
                'field' => 'client',
                'label' => 'Имя клиента',
                'rules' => 'trim|min_length[3]|max_length[25]|required|only_words|first_uppercase'
            ),
        array(
                'field' => 'serial',
                'label' => 'Номер счета',
                'rules' => 'trim|required|greater_than[1]|max_length[10]|integer|account_uniq_id'
            ),
        array(
                'field' => 'balance',
                'label' => 'Начальный баланс',
                'rules' => 'trim|required|transfer_valid_sum|greater_than[0]|max_length[10]'
            ),
    ),
    'account/transfer' => array(
        array(
                'field' => 'from',
                'label' => 'Откуда',
                'rules' => 'required'
            ),
        array(
                'field' => 'to',
                'label' => 'Куда',
                'rules' => 'required'
            ),
        array(
                'field' => 'sum',
                'label' => 'Сумма перевода',
                'rules' => 'required|transfer_valid_sum'
            ),
    ),     

);
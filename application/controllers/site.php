<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MY_Controller{

	function __construct(){
		parent::__construct();

	}

	public function index()
	{
		$this->tpl['content'] = $this->load->view('index', '', true);
		$this->_render();
	}
}
?>
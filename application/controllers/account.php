<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model("account_model");
	}

	public function create(){

		

		if(isset($_POST['serial'])) $this->form_validation->validationVars['account_uniq_id'] = $this->account_model->isUniqAccId($_POST['serial']);

		if ($this->form_validation->run() == FALSE)
		{
			$this->tpl['content'] = $this->load->view('account/create', '', true);
		}
		else
		{
			$this->account_model->add($_POST);

			$this->tpl['content'] = $this->load->view('messages/form_success_1', array(
				"msg" => $this->lang->line('msg_accountCreateSuccess'),
				"backLink" => site_url("createAccount"),
			), true);
		}
		
		$this->_render();
	}

	public function listOf(){

		//$this->load->model("account_model", "", TRUE);
		$accountsList = $this->account_model->getFormatList();

		$sysAccount = $this->account_model->getSysAccount();

		$this->tpl['content'] = $this->load->view('account/list', array(
			"accountsList" => $accountsList,
			"sysAccount" => $sysAccount,
		), true);
		$this->_render();
	}

	public function history($accountId){
		$accountInfo = $this->account_model->getAccount($accountId);
		if($accountInfo == FALSE) show_404();

		$accountHistory = $this->account_model->getFormatAccountHistory($accountId);

		$this->tpl['content'] = $this->load->view('account/history', array(
			"accountHistory" => $accountHistory,
			"accountInfo" => $accountInfo,
		), true);
		$this->_render();
	}

	public function transfer(){

		if ($this->form_validation->run() == FALSE)
		{
			$accountsList = $this->account_model->getFormatList();
			$this->tpl['content'] = $this->load->view('account/transfer', array(
				"accountsList" => $accountsList,
			), true);
		}
		else
		{
			$transferSum = number_format($_POST['sum'], 2, '.', '');
			$transferSum *= 100;

			$transferStatus = $this->account_model->transfer($_POST['from'], $_POST['to'], $transferSum);
			if(isset($transferStatus['error']))
			{
				switch($transferStatus['error'])
				{
					case ERR_TRANSFER_ACCOUNT_DEFICIENCY:
						$this->tpl['content'] = $this->load->view('messages/form_error_1', array(
							"msg" => $this->lang->line('msg_accountTransferErrorDeficiency'),
							"backLink" => site_url("transfer"),
						), true);
						break;

					case ERR_TRANSFER_ACCOUNT_OVERFLOW:
						$this->tpl['content'] = $this->load->view('messages/form_error_1', array(
							"msg" => $this->lang->line('msg_accountTransferErrorOverflow'),
							"backLink" => site_url("transfer"),
						), true);
						break;
				}
			}
			else
			{
				$this->tpl['content'] = $this->load->view('messages/form_success_1', array(
					"msg" => $this->lang->line('msg_accountTransferSuccess'),
					"backLink" => site_url("transfer"),
				), true);
			}
		}

		

		// /$this->account_model->transfer(123132, 100000, 100);

		$this->_render();
	}

}
?>
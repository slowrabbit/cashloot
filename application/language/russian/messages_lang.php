<?php

$lang['msg_accountCreateSuccess'] = "Новый счет успешно создан";

$lang['msg_accountTransferErrorDeficiency'] = "Недостаточно средств на счету! Операция отменена";
$lang['msg_accountTransferErrorOverflow'] = "Переполнение целевого счета! Операция отменена";
$lang['msg_accountTransferSuccess'] = "Перевод успешно выполнен";
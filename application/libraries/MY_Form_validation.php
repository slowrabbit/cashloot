<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class MY_Form_validation extends CI_Form_validation {

	function __construct($rules = array()){
		parent::__construct($rules);
		
	}

	var $validationVars;

	public function only_words($str)
	{
		$pattern = "/^[a-zA-Zа-яА-Я\s\"\']+$/u";
		if (preg_match($pattern, $str)) return TRUE;
		return FALSE;
	}

	public function first_uppercase($str)
	{
		if($str[0] === strtoupper($str[0])) return TRUE;
		return FALSE;
	}

	public function account_uniq_id($id)
	{
		return $this->validationVars['account_uniq_id'];
	}

	public function transfer_valid_sum($str)
	{
		$pattern = "/^\d*([\.]\d{2})*$/u";
		if (preg_match($pattern, $str)) return TRUE;
		return FALSE;
	}

}
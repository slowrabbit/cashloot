<nav class="navGlobal_b">
	<ul class="navGlobal_b-list">
		<li class="navGlobal_b-item">
			<?
			if($currentRoute == "site/index")
			{
				?>
				<span class="navGlobal_b-link active">Главная</span>
				<?
			}
			else
			{
				?>
				<a href="/" class="navGlobal_b-link">Главная</a>
				<?
			}
			?>
		</li>
		<li class="navGlobal_b-item">
			<?
			if($currentRoute == "account/create")
			{
				?>
				<span class="navGlobal_b-link active">Добавить счет</span>
				<?
			}
			else
			{
				?>
				<a href="<?=$link['create']?>" class="navGlobal_b-link">Добавить счет</a>
				<?
			}
			?>
		</li>
		<li class="navGlobal_b-item">
			<?
			if($currentRoute == "account/listof")
			{
				?>
				<span class="navGlobal_b-link active">Список счетов</span>
				<?
			}
			else
			{
				?>
				<a href="<?=$link['list']?>" class="navGlobal_b-link">Список счетов</a>
				<?
			}
			?>
		</li>
		<li class="navGlobal_b-item">
			<?
			if($currentRoute == "account/transfer")
			{
				?>
				<span class="navGlobal_b-link active">Сделать перевод</span>
				<?
			}
			else
			{
				?>
				<a href="<?=$link['transfer']?>" class="navGlobal_b-link">Сделать перевод</a>
				<?
			}
			?>
		</li>
	</ul>
	<!-- <div class="navGlobal_b-toggle"></div> -->
</nav>
<!doctype html>
<html lang="ru-RU">
	<?=$tpl['head']?>
	<body> 				
		<!-- main_b -->
<div class="main_b">

	<!-- header_b -->
	<div class="header_b">
		<div class="ctr">
			<!-- navGlobal_b -->
			<?=$tpl['menu']?>
			<!-- /navGlobal_b -->	
		</div>
	</div>
	<!-- /header_b -->

	<div class="main_b-wrap clearfix">
		<?=$tpl['content']?>
	</div>

</div>
<!-- /main_b -->
	<?=$tpl['footer']?>
	</body>
</html>
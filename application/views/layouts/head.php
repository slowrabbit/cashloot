<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>CashLoot v1.0</title>
	
	<script src="<?=base_url()?>html/lib/jquery-1.9.1.min.js"></script>
	<script src="<?=base_url()?>html/lib/jquery.dataTables.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?=base_url()?>html/js/helpers/html5shiv.js"></script>
		<script src="<?=base_url()?>html/js/helpers/IE9.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?=base_url()?>html/css/style.css" media="all">
	<link rel="stylesheet" href="<?=base_url()?>html/css/blocks.css" media="all">
	<link rel="stylesheet" href="<?=base_url()?>html/css/jqui.css" media="all">
	<link rel="stylesheet" href="<?=base_url()?>html/css/jquery.dataTables.css" media="all">
	
	<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
</head>
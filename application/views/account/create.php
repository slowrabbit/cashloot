<script type="text/javascript">
$(function(){
	$("#createAccountBtn").click(function(){
		$("#createAccountForm").submit();
	});
});
</script>

<!-- main_b -->
<div class="main_b">

	<!--#include virtual="/_header.html"-->

	<div class="main_b-wrap clearfix">
		<div class="ctr">
			<h1 class="main_b-heading">
				Создание счета
			</h1>	

			<div class="main_b-content">
				
				<form id="createAccountForm" method="post" class="formGlobal_b">
					<ul class="formGlobal_b-errorBox error">
						<?php echo validation_errors();?>
					</ul>
					<!-- <ul class="formGlobal_b-errorBox complete">
						<li>
							Gorrin
						</li>
						<li>
							Gorrin
						</li>	
						<li>
							Gorrin
						</li>
					</ul> -->
					<fieldset class="formGlobal_b-fieldset">
						<label class="formGlobal_b-label" for="formGlobal_b-name">
							Имя клиента:
						</label>
						<div class="formGlobal_b-inputWrap">
							<input id="formGlobal_b-name" class="inputText_b" type="text" name="client" value="<?php echo set_value('client');?>" placeholder="">	
						</div>		
						<label class="formGlobal_b-label" for="formGlobal_b-numberBill">
							Номер счета
						</label>
						<div class="formGlobal_b-inputWrap">
							<input id="formGlobal_b-numberBill" class="inputText_b" type="text" name="serial" value="<?php echo set_value('serial');?>" placeholder="">	
						</div>		
						<label class="formGlobal_b-label" for="formGlobal_b-balance">
							Начальный баланс
						</label>
						<div class="formGlobal_b-inputWrap">
							<input id="formGlobal_b-balance" class="inputText_b" type="text" name="balance" value="<?php echo set_value('balance');?>" placeholder="">	
						</div>		
						<span id="createAccountBtn" class="btn_b v1">Создать</span>
					</fieldset>
				</form>
			</div>

		</div>

	</div>

</div>
<!-- /main_b -->
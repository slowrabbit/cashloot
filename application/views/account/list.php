<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('.accounts').dataTable( {
		"paging": false,
        "info": false,
        "order": [[ 3, "desc" ]]
	} );
} );
</script>

<!-- main_b -->
<div class="main_b">

	<!--#include virtual="/_header.html"-->

	<div class="main_b-wrap clearfix">
		<div class="ctr">
			<h1 class="main_b-heading">
				Список счетов
			</h1>

			<div class="main_b-content">

				<?
				if($accountsList)
				{
					?>
					<table class="accounts display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Клиент</th>
								<th>Счет</th>
								<th>Баланс</th>
								<th>Дата Создания</th>
							</tr>
						</thead>

						<tfoot>
							<tr>
								<th>Клиент</th>
								<th>Счет</th>
								<th>Баланс</th>
								<th>Дата Создания</th>
							</tr>
						</tfoot>

						<tbody>
							<?
							foreach($accountsList as $account)
							{
								?>
								<tr>
									<td><a href="<?echo site_url("accounts/{$account['acc_id']}")?>"><?=$account['owner_name']?></a></td>
									<td><?=$account['acc_id']?></td>
									<td><?=$account['balance']?> $</td>
									<td><?=$account['create_time']?></td>
								</tr>
								<?
							}
							?>
							
						</tbody>
					</table>
					<?
				}
				?>
				
				<?
				if(isset($sysAccount['acc_id']))
				{
					$sysAccount['balance'] = $sysAccount['balance']/100;
					?>
					<a href="<?echo site_url("accounts/{$sysAccount['acc_id']}")?>">Системный счет</a>[<?=$sysAccount['acc_id']?>]: <?=$sysAccount['balance']?> $
					<?
				}
				?>
			</div>			
		</div>

		
	</div>

</div>
<!-- /main_b -->
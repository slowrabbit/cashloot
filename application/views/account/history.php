<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('.accounts').dataTable( {
		"paging": false,
        "info": false,

	} );
} );
</script>

<!-- main_b -->
<div class="main_b">

	<!--#include virtual="/_header.html"-->

	<div class="main_b-wrap clearfix">
		<div class="ctr">
			<h1 class="main_b-heading">
				История счета #<?=$accountInfo['acc_id']?>
			</h1>		
		

		<div class="main_b-content">
			<?
			if($accountHistory)
			{
				?>
				<table class="accounts display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Дата</th>
							<th>Номер счета</th>
							<th>Клиент</th>
							<th>Приход\Расход</th>
							<th>Остаток</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<th>Дата</th>
							<th>Номер счета</th>
							<th>Клиент</th>
							<th>Приход\Расход</th>
							<th>Остаток</th>
						</tr>
					</tfoot>

					<tbody>
						<?
						foreach($accountHistory as $accountHistoryItem)
						{
							?>
							<tr>
								<td><?=$accountHistoryItem['time']?></td>
								<td><?=$accountHistoryItem['src_acc_id']?></td>
								<td><?=$accountHistoryItem['owner_name']?></td>
								<td><?=$accountHistoryItem['sum']?> $</td>
								<td><?=$accountHistoryItem['balance']?> $</td>
							</tr>
							<?
						}
						?>
					</tbody>
				</table>
				<?
			}
			else
			{
				?>
				...Здесь будет выводится история счета...
				<?
			}
			?>
			
		</div>
		</div>
			
	</div>

</div>
<!-- /main_b -->
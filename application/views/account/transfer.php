<script type="text/javascript">
$(function(){
	$("#transferAccountBtn").click(function(){
		$("#transferAccountForm").submit();
	});
});
</script>

<!-- main_b -->
<div class="main_b">

	<!--#include virtual="/_header.html"-->

	<div class="main_b-wrap clearfix">
		<div class="ctr">
			<h1 class="main_b-heading">
				Перевод валюты
			</h1>	

			<div class="main_b-content">
				
				<form id="transferAccountForm" method="post" class="formGlobal_b">
					<ul class="formGlobal_b-errorBox error">
						<?php echo validation_errors();?>
					</ul>
					<!-- <ul class="formGlobal_b-errorBox complete">
						<li>
							Gorrin
						</li>
						<li>
							Gorrin
						</li>	
						<li>
							Gorrin
						</li>
					</ul> -->

					<fieldset class="formGlobal_b-fieldset">
						<label class="formGlobal_b-label" >
							Откуда:
						</label>
						<div class="formGlobal_b-inputWrap">
							<select name="from">
								<option disabled selected>Выберите плательщика</option>
								<?
								foreach($accountsList as $account)
								{
									?>
									<option value="<?=$account['acc_id']?>" <?php echo set_select("from", "{$account['acc_id']}");?>>[#<?=$account['acc_id']?>] <?=$account['owner_name']?>: $<?=$account['balance']?></option>
									<?
								}
								?>
							</select>
						</div>		
						<label class="formGlobal_b-label" >
							Куда:
						</label>
						<div class="formGlobal_b-inputWrap">
							<select name="to">
								<option disabled selected>Выберите получателя</option>
								<?
								foreach($accountsList as $account)
								{
									?>
									<option value="<?=$account['acc_id']?>" <?php echo set_select("to", "{$account['acc_id']}");?>>[#<?=$account['acc_id']?>] <?=$account['owner_name']?>: $<?=$account['balance']?></option>
									<?
								}
								?>
							</select>
						</div>		
						<label class="formGlobal_b-label" for="formGlobal_b-summ">
							Сумма перевода:
						</label>
						<div class="formGlobal_b-inputWrap">
							<input id="formGlobal_b-summ" class="inputText_b" type="text" name="sum" value="<?php echo set_value('sum');?>" placeholder="">	
						</div>	
						<span id="transferAccountBtn" class="btn_b v1">Перевести</span>
					</fieldset>
				</form>
			</div>		
		</div>

		

		<!-- <aside class="main_b-aside">
			
		</aside> -->
	</div>

</div>
<!-- /main_b -->
$(function() {
	var $html = $("html");

	/* tools */
	testMobileDevices(); // test for mobile devices
	testSizeScreen(); // test for screens size
		
	/* mobile menu */
	var $navGlobalList = $(".navGlobal_b-list"),
		$navGlobalToggle = $(".navGlobal_b-toggle");

	$navGlobalToggle.click(function() {
		if(!$navGlobalToggle.hasClass("open")) {
			$navGlobalToggle.addClass("open");
			$navGlobalList.addClass("open");
		}
		else {
			$navGlobalToggle.removeClass("open");
			$navGlobalList.removeClass("open");
		}
	});

	/* tabs content */
	$(".tabsContent_b").each(function(index, el) {
		var disabledArr = [];

		$(el).tabs({
			create: function(event, ui) {
				var $tabsContent_bNavItem = ui.tab.parents(".tabsContent_b-nav").find(".tabsContent_b-nav-item");

				$tabsContent_bNavItem.each(function(index, el) {
					var $tabsContent_b = $(el).parents(".tabsContent_b");

					if($(el).hasClass("disabled")) {
						disabledArr.push(index);
					}

					$tabsContent_b.tabs("option", "disabled", disabledArr);
				});
			}
		});	
	});	

	/* accordion content */
	$(".accordionContent_b").each(function(index, el) {
		$(el).accordion({
			header: ".accordionContent_b-switch",
			heightStyle: "content",
			active: false,
			collapsible: true
		});	
	});	
});
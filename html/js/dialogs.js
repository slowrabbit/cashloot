$(function() {
	var $dialogAny = $("#dialogAny"),
		$dialogAny_bOpen = $(".dialogAny_b-open"),
		$dialogsAll = $(".dialog_b"),
		$dialogsAll_open = $(".dialog_b-open");
		
	/* restart dialogs */
	$dialogsAll_open.click(function(e) {
		if(!$(this).hasClass("active")) {
			$dialogsAll.dialog("close");
		}	

        e.preventDefault();
	});

	/* open any dialog */	
	$dialogAny_bOpen.click(function(e) {
		var $this = $(this);

		if(!$this.hasClass("active")) {
			$this.addClass("active");
			$dialogAny.dialog("open");
		}
		else {
			$dialogAny.dialog("close");
		}
	});

	/* init any dialog */
	$dialogAny.dialog({
		modal: false,
		width: 430,
		autoOpen: true,
		resizable: false,
		position: {
			my: "right top+20",
			at: "right bottom",
			of: $dialogAny_bOpen
		},
		show: {
			effect: "fade",
			duration: 300
		},
		open: function() {
			var $overlay = $(".ui-widget-overlay"),
				$dialog_bClose;

			$overlay.addClass("bg dialogAny_b-close");

			$dialog_bClose = $(this).parents("body").find(".dialogAny_b-close");
	        $dialog_bClose.click(function() {
	        	$dialogEvents.dialog("close"); 
	            $overlay.removeClass("bg");
	        });
	    },
	    close: function() {
			$dialogAny_bOpen.removeClass("active");
	    }	
	});	
});	
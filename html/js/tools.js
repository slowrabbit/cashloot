/* test for mobile devices */
function testMobileDevices() {
	var $html = $("html");

	if(/iPad/i.test(navigator.userAgent)) {
		$html.addClass("iPad");
	}	
	if(/iPod/i.test(navigator.userAgent)) {
		$html.addClass("iPod");
	}
	if(/iPhone/i.test(navigator.userAgent)) {
		$html.addClass("iPhone");
	}
	if(/Android/i.test(navigator.userAgent)) {
		$html.addClass("android");
	}
	if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$html.addClass("mobile");
	}
}

/* test for screens size */
function testSizeScreen() {
	$(window).on("resize", function (e) {
	    if (document.documentElement.clientWidth <= 1024) {
	       
	    } else {
	        // elem.trigger("destroy"); for plugin destroy
	    }

	}).trigger("resize");
}
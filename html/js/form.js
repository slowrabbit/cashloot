$(function () {
	/* placeholder */
	$("input, textarea").placeholder();

	/* select box */
	var $customSelect = $(".customSelect_b");

	$customSelect.each(function(index, el) {
		$(el).selectize({
			onDropdownOpen: function($dropdown) {
				if(!$dropdown.hasClass("mCustomScrollbar")) {
					$dropdown.mCustomScrollbar();
				}
			}
		});
	});
});